using UnityEngine;

public class GameData
{
    public double timeAlive;
    public bool completedLevel;

    public GameData(double timeAlive, bool completedLevel)
    {
        this.timeAlive = timeAlive;
        this.completedLevel = completedLevel;
    }
}