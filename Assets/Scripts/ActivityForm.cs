using Unity;
using UnityEngine;
[System.Serializable]
public class ActivityForm
{
    public int gameDescriptor;
    public int dataProvider;
    public int miniGameUse;
    public float date;
    public int[] players;
    

    public PropertyInstance[] propertyInstances;

    public ActivityForm(PropertyInstance[] propertyInstances)
    {
        this.propertyInstances = propertyInstances;
    }
}