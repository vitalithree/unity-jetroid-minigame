using System.Collections.Generic;
using UnityEngine;

public class GameProperties : MonoBehaviour
{
    private Dictionary<string, PropertyInstance> propertyInstanceDictionary = new Dictionary<string, PropertyInstance>();

    public void Start()
    {
        CreateProperty(GamePropertyEnum.MINIGAME_LEVEL, "");
        CreateProperty(GamePropertyEnum.MINIGAME_TIME, "");
    }

    private void CreateProperty(GamePropertyEnum propertyTK, string value)
    {
        propertyInstanceDictionary.Add(GamePropertyEnumToStringMapper(propertyTK), new PropertyInstance(value, GamePropertyEnumToStringMapper(propertyTK)));
    }

    private string GamePropertyEnumToStringMapper(GamePropertyEnum gamePropertyEnum)
    {
        if( gamePropertyEnum == GamePropertyEnum.MINIGAME_LEVEL){
            return "COMPLEXITY";
        }else if(gamePropertyEnum == GamePropertyEnum.MINIGAME_TIME){
            return "MINIGAME_TIME";
        }else{
            return null;
        }
    }
    public void UpdatePropertyValueByPropertyTK(GamePropertyEnum propertyTK, string value)
    {
        propertyInstanceDictionary[GamePropertyEnumToStringMapper(propertyTK)].value = value;
    }

    public PropertyInstance[] GetPropertyInstanceArray()
    {
        PropertyInstance[] propertyInstancesArray = new PropertyInstance[propertyInstanceDictionary.Count];
        propertyInstanceDictionary.Values.CopyTo(propertyInstancesArray,0);
        return propertyInstancesArray;
    }
    
}