[System.Serializable]
public class PropertyInstance
{
    public string value;
    public string propertyTK;
    public int property;

    public PropertyInstance(string value = "", string propertyTK ="")
    {
        this.value = value;
        this.propertyTK = propertyTK;
    }   

}