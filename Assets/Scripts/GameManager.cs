using System;
using UnityEngine; 
using System.Runtime.InteropServices;

public class GameManager: MonoBehaviour
{ 
    [DllImport("__Internal")]
    private static extern void SendMessageToDoc(string message);
    
    private JSCommunicator jsCommunicator;
    private GameProperties gameProperties;
    private DataManager dataManager;

    void Start()
    {
        jsCommunicator =  gameObject.GetComponent<JSCommunicator>();
        gameProperties =  gameObject.GetComponent<GameProperties>();
        dataManager = GameObject.Find("DataManager").GetComponent<DataManager>();
    }

    public void ReceiveDataFromGameBus(string message = "")
    {
        Debug.Log("[IN UNITY GAME] " + dataManager.dataFromGameBus);
    }

    private string GamePropertyEnumToStringMapper(GamePropertyEnum gamePropertyEnum)
    {
        if( gamePropertyEnum == GamePropertyEnum.MINIGAME_LEVEL){
            return "COMPLEXITY";
        }else if(gamePropertyEnum == GamePropertyEnum.MINIGAME_TIME){
            return "MINIGAME_TIME";
        }else{
            return null;
        }
    }

    public void SendGameSessionDataToGameBus(double timeAlive, bool completedLevel)
    {
        Debug.Log("[IN UNITY GAME] START PROCESSING GAMEBUS DATA");
        string tempJson = SetFrameDataPropertyInstances(timeAlive, completedLevel);
        Debug.Log("[IN UNITY GAME] DATA SENDING TO GAMEBUS: " + tempJson);
        
        if(tempJson == ""){
            Debug.Log("[IN UNITY GAME] NO DATA FROM GAMEBUS!!");
        }else{
        jsCommunicator.SendMessageToGameBus(tempJson);
        }
    }

    public string SetFrameDataPropertyInstances(double timeAlive, bool completedLevel)
    {
        Debug.Log("[IN UNITY GAME] PROCESSING GAMEBUS DATA");
        if(dataManager.dataFromGameBus == ""){
            Debug.Log("[IN UNITY GAME] ERROR NO GAMEBUS DATA");   
            return "";
        }

        Debug.Log("[IN UNITY GAME] GAMEBUS DATA TO UNITY CLASS STARTED");
        Debug.Log("[IN UNITY GAME] GAMEBUS DATA TO PROCESS: " + dataManager.dataFromGameBus);
        FrameData frameData1 = JsonUtility.FromJson<FrameData>(dataManager.dataFromGameBus);
        Debug.Log("[IN UNITY GAME] GAMEBUS DATA TO UNITY CLASS COMPLETED");

        for(int i = 0; i < frameData1.activityForm.propertyInstances.Length; i++)
        {
            if(frameData1.activityForm.propertyInstances[i].propertyTK == GamePropertyEnumToStringMapper(GamePropertyEnum.MINIGAME_LEVEL))
            {
                frameData1.activityForm.propertyInstances[i].value = completedLevel.ToString();
            }
            else if(frameData1.activityForm.propertyInstances[i].propertyTK == GamePropertyEnumToStringMapper(GamePropertyEnum.MINIGAME_TIME))
            {
                frameData1.activityForm.propertyInstances[i].value = timeAlive.ToString();
            }
        }

        Debug.Log("[IN UNITY GAME] GAMEBUS PI SUCCESSFULLY UPDATED");
        Debug.Log("[IN UNITY GAME] GAMEBUS PI: " + frameData1.activityForm.propertyInstances.Length);

        frameData1.gameData = frameData1.activityForm.propertyInstances;
        string tempJson = JsonUtility.ToJson(frameData1);
        return tempJson;

    }
}