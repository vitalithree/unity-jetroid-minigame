using Unity;
using UnityEngine;
[System.Serializable]
public class FrameData 
{
    public ActivityForm activityForm;
    public PropertyInstance[] gameData;
    
    public FrameData(PropertyInstance[] gameData){
        this.gameData = gameData;
        this.activityForm = new ActivityForm(gameData);
    }

    public void SetPropertyInstances(PropertyInstance[] propertyInstances)
    {
        this.gameData = propertyInstances;
    }

}

