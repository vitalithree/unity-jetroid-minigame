using UnityEngine;
using System.Runtime.InteropServices;

public class DataManager : MonoBehaviour
{
    public string dataFromGameBus;
    private JSCommunicator jsCommunicator;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        jsCommunicator =  gameObject.GetComponent<JSCommunicator>();
        jsCommunicator.SendParentInitState();
    }

    public void GetDataFromGameBus(string data)
    {
        Debug.Log("[IN UNITY GAME] GOT THIS MESSAGE FROM HTML DOC: " + data);
        dataFromGameBus = data;
    }
}
