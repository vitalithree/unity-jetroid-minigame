﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienB : MonoBehaviour
{
    public AudioClip attackSound;

    private Animator animator;
    private bool readyToKill = false;
    private Explode explode;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "Player")
        {
            if(attackSound){
                AudioSource.PlayClipAtPoint(attackSound, transform.position);
            }
            readyToKill = true;
            explode = target.GetComponent<Explode>() as Explode;
            animator.SetInteger("AnimState", 1);
        }
    }

    private void OnTriggerExit2D(Collider2D target)
    {
        readyToKill = false;
        explode = null;
        animator.SetInteger("AnimState", 0);
    }

    void Kill()
    {
        if (readyToKill)
        {
            explode.OnExplode();
        }
    }
}
