mergeInto(LibraryManager.library, {

  SendMessageToParent: function (str) {
      window.parent.postMessage(Pointer_stringify(str), '*');
  },
  SendMessageToWindow: function(str){
        window.alert(Pointer_stringify(str));
  }, 
  NotifyParentOfInit: function(){
      window.postMessage(Pointer_stringify("Initialized"));
  }

});